# alpine-gateone

#### [alpine-x64-gateone](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-gateone/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinex64/alpine-x64-gateone/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinex64/alpine-x64-gateone/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinex64/alpine-x64-gateone/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-gateone)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-gateone)
#### [alpine-aarch64-gateone](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-gateone/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpineaarch64/alpine-aarch64-gateone/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpineaarch64/alpine-aarch64-gateone/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpineaarch64/alpine-aarch64-gateone/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64/alpine-aarch64-gateone)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64/alpine-aarch64-gateone)
#### [alpine-armhf-gateone](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-gateone/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinearmhf/alpine-armhf-gateone/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinearmhf/alpine-armhf-gateone/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinearmhf/alpine-armhf-gateone/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhf/alpine-armhf-gateone)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhf/alpine-armhf-gateone)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : Gate One
    - Gate One is an HTML5 web-based terminal emulator and SSH client



----------------------------------------
#### Run

```sh
docker run -d \
           -p 80:80/tcp \
           -e RUN_USER_NAME=<username> \
           -e RUN_USER_PASSWD=<passwd> \
           -v /conf.d:/conf.d \
           forumi0721alpine[ARCH]/alpine-[ARCH]-gateone:latest
```



----------------------------------------
#### Usage

* URL : [http://localhost:80/](http://localhost:80/)
    - Default username/password : admin/admin



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 80/tcp             | HTTP port                                        |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Config data                                      |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| RUN_USER_NAME      | Login username (default : forumi0721)            |
| RUN_USER_PASSWD    | Login password (default : passwd)                |
| RUN_USER_EPASSWD   | Login password (base64)                          |

