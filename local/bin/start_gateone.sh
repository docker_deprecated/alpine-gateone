#!/bin/sh

. /app/virtualenv/bin/activate

exec /app/virtualenv/bin/gateone "$@"

exit $?

